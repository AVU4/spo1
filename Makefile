compile:
	rm -rf ./build
	mkdir ./build
	gcc -Wall -g sources/*.c -o ./build/main -lm

build_shared_library:
	rm -rf ./build
	mkdir ./build
	gcc -fPIC  -Wall -g -c sources/filesystem_utils.c -lm
	gcc -fPIC  -Wall -g -c sources/output_util.c -lm
	gcc -fPIC  -Wall -g -c sources/operations_ext.c -lm
	gcc -fPIC  -Wall -g -c sources/partition.c -lm
	gcc -fPIC  -Wall -g -c sources/utils.c -lm
	cp filesystem_utils.o ./build/filesystem_utils.o
	cp operations_ext.o ./build/operations_ext.o
	cp partition.o ./build/partition.o
	cp utils.o ./build/utils.o
	cp output_util.o ./build/output_util.o
	rm *.o
	gcc -shared -lm -o libmy_ext.so build/*.o
